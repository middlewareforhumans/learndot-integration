package com.mulesoft.training.lms;

public class Learner {
	
	private String name;
	private String title;
	private String email;
	private String mobile;
	private String office;
	private String country;
	private String city;
	private String training;
	private String keyword;
	private int contentId;
	private int id;
	private int primaryCtaegoryId;
	private String status;
	private boolean rootType;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getOffice() {
		return office;
	}
	public void setOffice(String office) {
		this.office = office;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getTraining() {
		return training;
	}
	public void setTraining(String training) {
		this.training = training;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public int getContentId() {
		return contentId;
	}
	public void setContentId(int contentId) {
		this.contentId = contentId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPrimaryCtaegoryId() {
		return primaryCtaegoryId;
	}
	public void setPrimaryCtaegoryId(int primaryCtaegoryId) {
		this.primaryCtaegoryId = primaryCtaegoryId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public boolean isRootType() {
		return rootType;
	}
	public void setRootType(boolean rootType) {
		this.rootType = rootType;
	}

}
