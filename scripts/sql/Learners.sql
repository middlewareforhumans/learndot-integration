CREATE TABLE `Learners`
(
   name                  varchar(50),
   title                 varchar(30),
   organization          varchar(60),
   email                 varchar(40),
   mobile                varchar(30),
   `officePhone`         varchar(30),
   country               varchar(30),
   city                  varchar(30),
   training              varchar(60),
   `enrollmentDate`      datetime,
   `completionDate`      datetime,
   keyword               varchar(20),
   `contentId`           int(10),
   id                    int(10),
   `primaryCategoryId`   int(10),
   status                varchar(30),
   `rootType`            tinyint(1),
   `trainingId`          int(10)
)